package ph.shaexpi.php.ph.view.genre.showalllist;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;
import ph.shaexpi.php.ph.view.genre.showindividual.viewlesbian;

/**
 * Created by on 9/30/2016.
 */

public class lesbiangenre extends ListActivity {

    ListView list;

    private ProgressDialog progressDialog;

    TextView categorytext;

    JSONParser jsonParser = new JSONParser();

    ArrayList<HashMap<String, String>> lesbianTitle;

    private static String url_lesbian_stories = "http://shaexpii.esy.es/getalllesbian.php";

    private static String TAG_SUCCESS = "success";
    private static String TAG_STORIES = "lesbian";
    private static String TAG_ID = "id";
    private static String TAG_TITLE = "title";

    JSONArray campuss = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        lesbianTitle = new ArrayList<HashMap<String, String>>();

        categorytext = (TextView)findViewById(R.id.categorytext);
        categorytext.setTypeface(constant.font(lesbiangenre.this));
        categorytext.setTextSize(20);
        categorytext.setText("Lesbian");

        list = getListView();

        new loadAllStories().execute(url_lesbian_stories);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long idd) {
                String id = ((TextView)view.findViewById(R.id.id)).getText().toString();
                Intent in = new Intent(getApplicationContext(), viewlesbian.class);
                in.putExtra(TAG_ID, id);
                startActivityForResult(in, 100);
            }
        });
    }

    class loadAllStories extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(lesbiangenre.this);
            progressDialog.setMessage("Loading Stories. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(final String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jsonParser.makeHttpRequest(args[0], "GET", params);

            // Check your log cat for JSON response
            Log.d("All Stories: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    campuss = json.getJSONArray(TAG_STORIES);

                    // looping through All Products
                    for (int i = 0; i < campuss.length(); i++) {
                        JSONObject c = campuss.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String title = c.getString(TAG_TITLE);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_ID, id);
                        map.put(TAG_TITLE, title);

                        // adding HashList to ArrayList
                        lesbianTitle.add(map);
                    }
                } else {
                    // no products found
                    // Launch Add New product Activity
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            lesbiangenre.this, lesbianTitle,
                            R.layout.campus, new String[] { TAG_ID,
                            TAG_TITLE},
                            new int[] { R.id.id, R.id.title });
                    // updating listview
                    setListAdapter(adapter);
                }
            });
        }
    }
}
