package ph.shaexpi.php.ph.view.genre.showalllist;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;
import ph.shaexpi.php.ph.view.genre.showindividual.viewstorygay;

/**
 * Created by on 9/30/2016.
 */

public class gaygenre extends ListActivity {

    ListView list;

    TextView categorytext;

    private ProgressDialog progressDialog;

    JSONParser jsonParser = new JSONParser();

    ArrayList<HashMap<String, String>> gayTitle;

    private static String url_gay_stories = "http://shaexpii.esy.es/getallgay.php";

    private static String TAG_SUCCESS = "success";
    private static String TAG_STORIES = "gay";
    private static String TAG_ID = "id";
    private static String TAG_TITLE = "title";

    JSONArray gay = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        gayTitle = new ArrayList<HashMap<String, String>>();

        categorytext = (TextView)findViewById(R.id.categorytext);
        categorytext.setTypeface(constant.font(gaygenre.this));
        categorytext.setTextSize(20);
        categorytext.setText("Gay");

        list = getListView();

        new loadAllStories().execute(url_gay_stories);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long idd) {
                String id = ((TextView)view.findViewById(R.id.id)).getText().toString();
                Intent in = new Intent(getApplicationContext(), viewstorygay.class);
                in.putExtra(TAG_ID, id);
                startActivityForResult(in, 100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 100){
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
    }

    class loadAllStories extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(gaygenre.this);
            progressDialog.setMessage("Loading Stories. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jsonParser.makeHttpRequest(args[0], "GET", params);
            Log.d("All Stories: ", json.toString());
            try{
                int success = json.getInt(TAG_SUCCESS);

                if(success == 1){
                    gay = json.getJSONArray(TAG_STORIES);

                    for(int i = 0; i < gay.length(); i++){
                        JSONObject c = gay.getJSONObject(i);

                        String id = c.getString(TAG_ID);
                        String title = c.getString(TAG_TITLE);

                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(TAG_ID, id);
                        map.put(TAG_TITLE, title);

                        gayTitle.add(map);
                    }
                }else{
                    // if there's an error or empty stories
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            progressDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            gaygenre.this, gayTitle,
                            R.layout.campus, new String[] { TAG_ID,
                            TAG_TITLE},
                            new int[] { R.id.id, R.id.title });
                    // updating listview
                    setListAdapter(adapter);
                }
            });
        }
    }
}
