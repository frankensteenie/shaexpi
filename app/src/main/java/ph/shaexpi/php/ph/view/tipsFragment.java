package ph.shaexpi.php.ph.view;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.devspark.appmsg.AppMsg;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.aboutus;
import ph.shaexpi.php.ph.fitness;
import ph.shaexpi.php.ph.hiv;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;
import ph.shaexpi.php.ph.safe;
import ph.shaexpi.php.ph.safedays;
import ph.shaexpi.php.ph.stamina;
import ph.shaexpi.php.ph.things;

/**
 * Created on 7/15/2016.
 */
public class tipsFragment extends Fragment {

    TextView tiptext;
    ProgressDialog pDialog;
    View v;
    JSONParser jsonParser = new JSONParser();
    private static String urltipall = "http://shaexpii.esy.es/gettip.php";
    String id;

    private static String TAG_SUCCESS = "success";
    private static String TAG_TIP = "tip";
    private static String TAG_TIP_PUSH = "tippush";

    AppMsg dialog;

    JSONArray tips;

    Button about, hivbt, safebt, fitnessbtn, staminabtn, thingsbtn, safebtn;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tipsfragment, container, false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        about = (Button)v.findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), aboutus.class);
                startActivity(i);
            }
        });

        hivbt = (Button) v.findViewById(R.id.hiv);
        hivbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), hiv.class);
                startActivity(i);
            }
        });

        safebt = (Button)v.findViewById(R.id.safe);
        safebt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), safe.class);
                startActivity(i);
            }
        });

        fitnessbtn = (Button)v.findViewById(R.id.fitness);
        fitnessbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), fitness.class);
                startActivity(i);
            }
        });

        staminabtn = (Button)v.findViewById(R.id.Stamina);
        staminabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), stamina.class);
                startActivity(i);
            }
        });

        thingsbtn = (Button)v.findViewById(R.id.things);
        thingsbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), things.class);
                startActivity(i);
            }
        });

        safebtn = (Button)v.findViewById(R.id.safedays);
        safebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), safedays.class);
                startActivity(i);
            }
        });

        new showtip().execute(urltipall);
        return v;
    }

    class showtip extends AsyncTask<String, String, String>{

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading Tips. Please wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(final String... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONObject json = jsonParser.makeHttpRequest(args[0], "GET", params);

                    try{
                        int success = json.getInt(TAG_SUCCESS);
                        if(success == 1){
                            tips = json.getJSONArray(TAG_TIP);
                            for(int i = 0; i < tips.length(); i++){
                                JSONObject c = tips.getJSONObject(i);

                                String tip = c.getString(TAG_TIP_PUSH);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(TAG_TIP_PUSH, tip);

                                tiptext = (TextView)v.findViewById(R.id.tiptext);
                                tiptext.setTypeface(constant.font(getContext()));
                                tiptext.setTextSize(30);

                                tiptext.setText(tip);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            pDialog.dismiss();
        }
    }
}
