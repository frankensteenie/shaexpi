package ph.shaexpi.php.ph.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.baseAdapterGrid;
import ph.shaexpi.php.ph.view.genre.showalllist.campusgenre;
import ph.shaexpi.php.ph.view.genre.showalllist.fantasgenre;
import ph.shaexpi.php.ph.view.genre.showalllist.gaygenre;
import ph.shaexpi.php.ph.view.genre.showalllist.groupgenre;
import ph.shaexpi.php.ph.view.genre.showalllist.lesbiangenre;
import ph.shaexpi.php.ph.view.genre.showalllist.onenightgenre;

/**
 * Created on 7/15/2016.
 */
public class categoryFragment extends Fragment {

    View v;
    FragmentTransaction ft;
    GridView grid;
    String[] categoryString = {
            "Campus / Office","Fantasy","Gay","Group","Lesbian","One Night Stand"
    };
    int[] imageId = {
            R.mipmap.action,
            R.mipmap.comedy,
            R.mipmap.drama,
            R.mipmap.horror,
            R.mipmap.scifi,
            R.mipmap.scifi,
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.create_fragment_grid_base, container, false);

        ft = getActivity().getSupportFragmentManager().beginTransaction();

        baseAdapterGrid adapter = new baseAdapterGrid(getContext(), categoryString, imageId);
        grid = (GridView)v.findViewById(R.id.gridView);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        Intent campus = new Intent(getContext(), campusgenre.class);
                        startActivity(campus);
                        break;
                    case 1:
                        Intent comedy = new Intent(getContext(), fantasgenre.class);
                        startActivity(comedy);
                        break;
                    case 2:
                        Intent horror = new Intent(getContext(), gaygenre.class);
                        startActivity(horror);
                        break;
                    case 3:
                        Intent action = new Intent(getContext(), groupgenre.class);
                        startActivity(action);
                        break;
                    case 4:
                        Intent nonfiction = new Intent(getContext(), lesbiangenre.class);
                        startActivity(nonfiction);
                        break;
                    case 5:
                        Intent fiction = new Intent(getContext(), onenightgenre.class);
                        startActivity(fiction);
                        break;
                }
            }
        });
        return v;
    }
}
