package ph.shaexpi.php.ph.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ph.shaexpi.php.ph.R;

/**
 * Created on 7/15/2016.
 */
public class messageFragment extends Fragment  {

    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.messagesfragment, container, false);
        return v;
    }
}
