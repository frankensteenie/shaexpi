package ph.shaexpi.php.ph;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.VideoView;

/**
 * Created by on 10/7/2016.
 */

public class hiv extends Activity {

    VideoView view;
    MediaController mediaController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hiv);

        if(mediaController == null){
            mediaController = new MediaController(hiv.this);
        }

        view = (VideoView)findViewById(R.id.video);
        try{
            view.setMediaController(mediaController);
            view.setVideoURI(Uri.parse("android.resource://" + hiv.this.getPackageName() + "/" + R.raw.hiv));
            view.start();
        }catch (Exception e){
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }
}
