package ph.shaexpi.php.ph.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ph.shaexpi.php.ph.R;

/**
 * Created on 8/19/2016.
 */
public class baseAdapterGrid extends BaseAdapter {

    private Context mContext;
    private final String[] stringCategory;
    private final int[] ImageId;

    public baseAdapterGrid(Context c, String[] stringCategory, int[] imageId){
        mContext = c;
        this.ImageId = imageId;
        this.stringCategory = stringCategory;
    }

    @Override
    public int getCount() {
        return stringCategory.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(view == null){
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.create_fragment_grid_content, null);
            TextView textView = (TextView)grid.findViewById(R.id.grid_text);
            ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
            textView.setText(stringCategory[position]);
            textView.setTypeface(constant.font(constant.context));
            imageView.setImageResource(ImageId[position]);
        }else{
            grid = (View)view;
        }
        return grid;
    }
}
