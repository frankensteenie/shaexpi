package ph.shaexpi.php.ph.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.devspark.appmsg.AppMsg;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;

public class createFragment extends Fragment implements View.OnClickListener{

    public createFragment(){
        //empty constructor

    }

    View v;
    EditText titleStory, storyStory;
    String titleString;
    String storyString;
    Button createStory, delete;
    //Button categories
    Button campus, fantasy, gay, group, lesbian, onenight;
    //end Button categories
    Dialog dialogCreate, dialoginner;
    public static HashMap<String, String> postDatacampus = new HashMap<String, String>();

    String TAG = "createFragment";

    ArrayList<String> foulwords;

    JSONParser jsonParser = new JSONParser();
    String TAG_SUCCESS = "success";
    AppMsg dialog;
    String url_create_campus_story = "http://shaexpii.esy.es/campus.php";
    String url_create_fantasy_story = "http://shaexpii.esy.es/fantasy.php";
    String url_create_gay_story = "http://shaexpii.esy.es/gay.php";
    String url_create_group_story = "http://shaexpii.esy.es/group.php";
    String url_create_lesbian_story = "http://shaexpii.esy.es/lesbian.php";
    String url_create_onenight_sotry = "http://shaexpii.esy.es/onenight.php";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.createfragment, container, false);

        //TextViews
        titleStory = (EditText) v.findViewById(R.id.titleOfStory);
        titleStory.setHint("Title here");
        storyStory = (EditText) v.findViewById(R.id.showStory);
        storyStory.setHint("Your Story here");
        //end of TextViews

        //Button
        createStory = (Button)v.findViewById(R.id.create_story);
        createStory.setText("Create Story");
        createStory.setTypeface(constant.font(getContext()));
        createStory.setOnClickListener(this);

        delete = (Button)v.findViewById(R.id.delete);
        delete.setText("Delete");
        delete.setOnClickListener(this);

        foulwords = new ArrayList<String>();
        foulwords.contains(constant.foulWords[0]);
        foulwords.contains(constant.foulWords[1]);
        foulwords.contains(constant.foulWords[2]);
        foulwords.contains(constant.foulWords[3]);
        foulwords.contains(constant.foulWords[4]);

        storyString = storyStory.getText().toString();

        //end of Button
        return v;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.create_story:
                String[] arr = storyString.split(" ");
                for(String ss : arr){
                    if(Arrays.asList(constant.foulWords).contains(ss)){
//                        dialog.makeText((Activity)getContext(), "This will be evaluated by admin", AppMsg.STYLE_ALERT).show();
                    }
                }
                showDialog();
                break;
            case R.id.delete:
                new AlertDialog.Builder(getContext())
                        .setTitle("Delete story and title")
                        .setMessage("Are you sure you want to delete the story?")
                        .setNegativeButton("No", null)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                titleStory.setText("");
                                storyStory.setText("");
                            }
                        }).create().show();
                break;

        }
    }

    void showDialog(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            dialogCreate = new Dialog(getContext(), android.R.style.Theme_Holo_Light_Dialog);
        }else{
            dialogCreate = new Dialog(getContext());
        }
        dialogCreate.setTitle("Choose a category");
        dialogCreate.setContentView(R.layout.categorypop);
        campus = (Button)dialogCreate.findViewById(R.id.campus);
        campus.setText("Campus / Office");
        campus.setTypeface(constant.font(getContext()));
        campus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleString = titleStory.getText().toString();
                storyString = storyStory.getText().toString();
                if(storyString.length() >= 300){
                    dialog.makeText((Activity)getContext(), "Please do not exceed to 300 characters", AppMsg.STYLE_ALERT).show();
                }else{
                    new campuscreatestory().execute(url_create_campus_story);
                }
                dialogCreate.dismiss();
            }
        });

        fantasy = (Button)dialogCreate.findViewById(R.id.fantasy);
        fantasy.setText("Fantasy");
        fantasy.setTypeface(constant.font(getContext()));
        fantasy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleString = titleStory.getText().toString();
                storyString = storyStory.getText().toString();
                if(storyString.length() >= 300){
                    dialog.makeText((Activity)getContext(), "Please do not exceed to 300 characters", AppMsg.STYLE_ALERT).show();
                }else{
                    new campuscreatestory().execute(url_create_fantasy_story);
                }
                dialogCreate.dismiss();
            }
        });

        gay = (Button)dialogCreate.findViewById(R.id.gay);
        gay.setText("Gay");
        gay.setTypeface(constant.font(getContext()));
        gay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleString = titleStory.getText().toString();
                storyString = storyStory.getText().toString();
                if(storyString.length() >= 300){
                    dialog.makeText((Activity)getContext(), "Please do not exceed to 300 characters", AppMsg.STYLE_ALERT).show();
                }else{
                    new campuscreatestory().execute(url_create_gay_story);
                }
                dialogCreate.dismiss();
            }
        });

        group = (Button)dialogCreate.findViewById(R.id.group);
        group.setText("Group");
        group.setTypeface(constant.font(getContext()));
        group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleString = titleStory.getText().toString();
                storyString = storyStory.getText().toString();
                if(storyString.length() >= 300){
                    dialog.makeText((Activity)getContext(), "Please do not exceed to 300 characters", AppMsg.STYLE_ALERT).show();
                }else{
                    new campuscreatestory().execute(url_create_group_story);
                }
                dialogCreate.dismiss();
            }
        });

        lesbian = (Button)dialogCreate.findViewById(R.id.lesbian);
        lesbian.setText("Lesbian");
        lesbian.setTypeface(constant.font(getContext()));
        lesbian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleString = titleStory.getText().toString();
                storyString = storyStory.getText().toString();
                if(storyString.length() >= 300){
                    dialog.makeText((Activity)getContext(), "Please do not exceed to 300 characters", AppMsg.STYLE_ALERT).show();
                }else{
                    new campuscreatestory().execute(url_create_lesbian_story);
                }
                dialogCreate.dismiss();
            }
        });

        onenight = (Button)dialogCreate.findViewById(R.id.onenight);
        onenight.setText("One Night Stand");
        onenight.setTypeface(constant.font(getContext()));
        onenight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                titleString = titleStory.getText().toString();
                storyString = storyStory.getText().toString();
                if(storyString.length() >= 300){
                    dialog.makeText((Activity)getContext(), "Please do not exceed to 300 characters", AppMsg.STYLE_ALERT).show();
                }else{
                    new campuscreatestory().execute(url_create_onenight_sotry);
                }
                dialogCreate.dismiss();
            }
        });

        dialogCreate.show();
    }

//    String checker() {
//        String check = storyStory.getText().toString();
//        for (int i = 0; i < constant.foulWords.length; i++) {
//            for (int k = 0; k < constant.tagalogFoul.length; k++) {
//                if (check.contains(constant.foulWords[i])) {
//                    dialog.makeText((Activity) getContext(), "You used a foul words", AppMsg.STYLE_ALERT).show();
//                } else if (check.contains(constant.tagalogFoul[k])) {
//                    dialog.makeText((Activity) getContext(), "You used a foul words", AppMsg.STYLE_ALERT).show();
//                }
//            }
//        }
//        return check;
//    }

    class campuscreatestory extends AsyncTask<String, String, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialoginner = new ProgressDialog(getContext());
            dialoginner.setTitle("Creating Story");
            dialoginner.setCancelable(false);
            dialoginner.show();
        }

        @Override
        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("title", titleString));
            params.add(new BasicNameValuePair("story", storyString));

            //getting JSON Object
            JSONObject json = jsonParser.makeHttpRequest(args[0], "POST", params);
            Log.d("Create Campus Response ", json.toString());
            try{
                int success = json.getInt(TAG_SUCCESS);
                if(success == 1){
//                    Toast.makeText(getContext(), "Created Story", Toast.LENGTH_SHORT).show();
                    Log.d("Created Story: ", "success");
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            dialoginner.dismiss();
            dialog.makeText((Activity)getContext(), "Successfully created story: " + titleStory.getText().toString(), AppMsg.STYLE_INFO).show();
            titleStory.setHint("Title here");
            createStory.setText("Create Story");
        }
    }
}