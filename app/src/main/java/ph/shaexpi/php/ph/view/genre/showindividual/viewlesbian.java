package ph.shaexpi.php.ph.view.genre.showindividual;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.devspark.appmsg.AppMsg;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;

/**
 * Created by on 9/30/2016.
 */

public class viewlesbian extends Activity {

    TextView title, story, commentwelcome, commentsection, date;
    EditText commentedittext;
    Button btncomment;

    String commentsToString;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    String id;

    private static String urllesbian = "http://shaexpii.esy.es/getlesbianinstance.php";
    private static String campuscommentappend = "http://shaexpi.esy.es/campus.php";

    private static String TAG_SUCCESS = "success";
    private static String TAG_CAMPUS = "lesbian";
    private static String TAG_ID = "id";
    private static String TAG_TITLE = "title";
    private static String TAG_STORY = "story";
    private static String TAG_COMMENT = "comment";
    private static String TAG_DATE = "dateadded";

    AppMsg dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.read);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        commentwelcome = (TextView)findViewById(R.id.commentwelcome);
        commentwelcome.setText("Comment Section");
        commentwelcome.setTypeface(constant.font(getApplicationContext()));
        commentwelcome.setTextSize(25);

        commentedittext = (EditText)findViewById(R.id.commentedittext);

        Intent i = getIntent();
        id = i.getStringExtra(TAG_ID);

        new storyfull().execute(urllesbian);

        btncomment = (Button)findViewById(R.id.btncomment);
        btncomment.setText("Send comment");
        btncomment.setTypeface(constant.font(viewlesbian.this));
        btncomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentsToString = commentedittext.getText().toString();
                new sendcomment().execute(campuscommentappend);
            }
        });
    }

    class storyfull extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(viewlesbian.this);
            pDialog.setMessage("Loading Story. Please wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(final String... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int success;
                    try{
                        List<NameValuePair> paramss = new ArrayList<NameValuePair>();
                        paramss.add(new BasicNameValuePair("id", id));

                        JSONObject json = jsonParser.makeHttpRequest(args[0], "GET", paramss);
                        Log.d("Single Story Details", json.toString());

                        success = json.getInt(TAG_SUCCESS);

                        if(success == 1){
                            JSONArray storyObj = json.getJSONArray(TAG_CAMPUS);

                            JSONObject campusStory = storyObj.getJSONObject(0);

                            title = (TextView)findViewById(R.id.title);
                            title.setTypeface(constant.font(viewlesbian.this));
                            title.setTextSize(25);
                            story = (TextView)findViewById(R.id.story);
                            story.setTypeface(constant.font(viewlesbian.this));
                            story.setTextSize(15);

                            date = (TextView)findViewById(R.id.date);
                            date.setTypeface(constant.font(viewlesbian.this));
                            date.setTextSize(20);

                            title.setText(campusStory.getString(TAG_TITLE));
                            story.setText(campusStory.getString(TAG_STORY));
                            date.setText("Date Added: " + campusStory.getString(TAG_DATE));
                        }else{
                            dialog.makeText(viewlesbian.this, "Story not found", AppMsg.STYLE_INFO).show();
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog.dismiss();
            dialog.makeText(viewlesbian.this, "Success get story: " + title.getText().toString(), AppMsg.STYLE_INFO).show();

            constant.playSound();
        }
    }

    class sendcomment extends AsyncTask<String, String, String>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(viewlesbian.this);
            pDialog.setMessage("Adding comment. Please wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            Log.d("ID: ", id);
            Log.d("Comment Post: ", TAG_COMMENT + commentsToString);
            try{
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                JSONObject json = jsonParser.makeHttpRequest(args[0], "POST", params);
                params.add(new BasicNameValuePair(TAG_ID, id));
                Log.d("comment response ", json.toString());
                int success = json.getInt(TAG_SUCCESS);
                if(success == 1){

                    params.add(new BasicNameValuePair(TAG_COMMENT, commentsToString));

                    Log.d("Added comment success", commentsToString);
                    Intent i = getIntent();
                    setResult(100, i);
                    finish();
                }else{
                    //failed to update
                }
            }catch (JSONException e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            dialog.makeText(viewlesbian.this, "Added comment", AppMsg.STYLE_INFO).show();
            pDialog.dismiss();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        constant.mp.stop();
    }
}
