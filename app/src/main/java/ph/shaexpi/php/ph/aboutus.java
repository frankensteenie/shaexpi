package ph.shaexpi.php.ph;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import ph.shaexpi.php.ph.model.constant;

/**
 * Created by on 10/7/2016.
 */

public class aboutus extends Activity {

    TextView aboutus, desc, title, desc2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);

        aboutus = (TextView)findViewById(R.id.aboutus);
        aboutus.setTypeface(constant.font(aboutus.this));
        aboutus.setTextSize(40);
        aboutus.setText("ShaExpi's Mission and Vision");

        desc = (TextView)findViewById(R.id.desc);
        desc.setTypeface(constant.font(aboutus.this));
        desc.setText("ShaExpi was founded in December, 2015 as a result of a simple observation: people have transformed how they live, work, shop, and buy, but \n" +
                "Story Telling through sharing of experiences have not adapted. With our powerful, easy to use, integrated application, story telling can \n" +
                "attract, engage, and delight end users by delivering inbound, personal mature experiences that are relevant, helpful, and personalized. ShaExpi is, after all, \n" +
                "on a mission to make the world more inbound, one story transformation after another. Application's mission is to make this type of application more interactive, \n" +
                "personal and possibly useful to one's everyday lives. In addition to this, to make a matured forum more private and secure since the said application uses credentials \n" +
                "which is uniquely created for individual user. ");

        title = (TextView)findViewById(R.id.title);
        title.setTypeface(constant.font(aboutus.this));
        title.setTextSize(40);
        title.setText("About US");

        desc2 = (TextView)findViewById(R.id.desc2);
        desc2.setTypeface(constant.font(aboutus.this));
        desc2.setText("ShaExpi was derived from the term Shared Experience and started to develop by the idea when in a lecture while in the middle of the class. \n" +
                "The topic was started about the whereabouts of the developers when they heared the running jokes about adult topics. When the proponents came up with the topic, \n" +
                "proponents thought of creating prosposal about adult contents specifically about sharing one's experiences. The proponents then submitted the proposal to their mentor \n" +
                "and since the prosposal was a bit out of the box compared to other's proposal, it was given a go and then ShaExpi materialized. The proponents then gathered different \n" +
                "ideas by brainstorming, sharing and exchanging thoughts using different resources and expertise of people who knows a lot about the said matter then ShaExpi happened.");
    }
}
