package ph.shaexpi.php.ph.view.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devspark.appmsg.AppMsg;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.HashMap;

import ph.shaexpi.php.ph.MainActivity;
import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.constant;

public class login extends Fragment implements OnClickListener, AsyncResponse{

    View v;
    EditText username, passwordE;
    Button loginButton, registerButton, logoutBtn;
    boolean isLogin;
    FragmentTransaction ft;
    AppMsg dialog;
    HashMap<String, String> postData = new HashMap<String, String>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.start_login, container, false);

        if(isLogin){
            Intent mainActivity = new Intent(getContext(), MainActivity.class);
            startActivity(mainActivity);
            getActivity().finish();
        }

        ft = getActivity().getSupportFragmentManager().beginTransaction();

        username = (EditText)v.findViewById(R.id.username);
        username.setTypeface(constant.font(getContext()));
        username.setHint("Username");

        passwordE = (EditText)v.findViewById(R.id.passwordE);
        passwordE.setTypeface(constant.font(getContext()));
        passwordE.setHint("Password");
        passwordE.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        loginButton = (Button)v.findViewById(R.id.loginBtn);
        loginButton.setTypeface(constant.font(getContext()));
        loginButton.setOnClickListener(this);

        registerButton = (Button)v.findViewById(R.id.registerBtn);
        registerButton.setTypeface(constant.font(getContext()));
        registerButton.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginBtn:
                postData.put("btnLogin", "loginButton");
                postData.put("mobile", "android");
                postData.put("txtUsername", username.getText().toString());
                postData.put("txtPassword", passwordE.getText().toString());

                PostResponseAsyncTask task = new PostResponseAsyncTask(getContext(), postData, this);
                task.execute("http://shaexpii.esy.es/login.php");
                break;

            case R.id.registerBtn:
                register rg = new register();
                ft.replace(R.id.fragment_changer, rg);
                ft.addToBackStack(null);
                ft.commit();
                break;
        }
    }

    @Override
    public void processFinish(String result) {
        if(result.equals("success")){
            isLogin = true;
            dialog.makeText((Activity) getContext(), "Successful Login", AppMsg.STYLE_INFO).show();
            Intent mainActivity = new Intent(getContext(), MainActivity.class);
            startActivity(mainActivity);
        }else{
            Toast.makeText(getContext(), "Login Failed", Toast.LENGTH_SHORT).show();
        }
    }
}