package ph.shaexpi.php.ph.view.start;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devspark.appmsg.AppMsg;
import com.kosalgeek.genasync12.AsyncResponse;
import com.kosalgeek.genasync12.PostResponseAsyncTask;

import java.util.HashMap;
import java.util.regex.Pattern;

import ph.shaexpi.php.ph.R;

/**
 * Created by on 9/23/2016.
 */

public class register extends Fragment implements View.OnClickListener, AsyncResponse {

    View v;
    EditText alias, username, password, email, password2;
    Button send;
    CharSequence message = "invalid email only gmail, ymail, yahoo";
    AppMsg dialog;
    FragmentTransaction ft;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.start_register, container, false);

        ft = getActivity().getSupportFragmentManager().beginTransaction();

        alias = (EditText)v.findViewById(R.id.alias);
        username = (EditText)v.findViewById(R.id.username);
        password = (EditText)v.findViewById(R.id.password);
        password2 = (EditText)v.findViewById(R.id.password2);
        email = (EditText)v.findViewById(R.id.email);
        send = (Button)v.findViewById(R.id.submit);

        alias.setHint("Alias");
        username.setHint("Desired Username");
        password.setHint("Desired Password");
        password2.setHint("Confirm Password");
        email.setHint("Your Email");
        send.setText("Register");

        send.setOnClickListener(this);

        v.setFocusable(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK){
                    login lg = new login();
                    ft.replace(R.id.fragment_changer, lg);
                    ft.commit();
                    return true;
                }else{
                    return false;
                }
            }
        });
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit:
                String passworccheck = password.getText().toString();
                if(password2.getText().toString().equals(passworccheck)){
                    registerMe();
                }else{
                    Toast.makeText(getContext(), "Password did not match", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void registerMe(){
        HashMap<String, String> postDataRegister = new HashMap<String, String>();
        postDataRegister.put("btnSubmit", "send");
        postDataRegister.put("mobile", "android");
        postDataRegister.put("txtAlias", alias.getText().toString());
        postDataRegister.put("txtUsername", username.getText().toString());
        postDataRegister.put("txtPassword", password.getText().toString());
        postDataRegister.put("txtEmail", email.getText().toString());
        PostResponseAsyncTask taskRegister = new PostResponseAsyncTask(getContext(), postDataRegister, this);
        taskRegister.execute("http://shaexpii.esy.es/register.php");
    }

    @Override
    public void processFinish(String result) {
        if(result.equals("successful")){
            dialog.makeText((Activity) getContext(), "Please wait for approval", AppMsg.STYLE_INFO).show();
            login lg = new login();
            ft.replace(R.id.fragment_changer, lg);
            ft.commit();
        }else{
            dialog.makeText((Activity) getContext(), "Please wait for approval", AppMsg.STYLE_INFO).show();
        }

    }

    private boolean isValidEmaillId(String email){
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
}
