package ph.shaexpi.php.ph.model;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import ph.shaexpi.php.ph.R;

/**
 * Created on 8/10/2016.
 */
public class imageSelector extends Activity {

    private static int IMG_RESULT = 1;
    String ImageDecode;
    Intent i;
    String[] file;
    ImageView imageselected;

    public ImageView getImageselected() {
        return imageselected;
    }

    public void setImageselected(ImageView imageselected) {
        this.imageselected = imageselected;
    }

    Button setImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageselector);

        imageselected = (ImageView)findViewById(R.id.selectImage);

        setImage = (Button)findViewById(R.id.selectimagebutton);
        setImage.setText("Open Gallery");
        setImage.setTypeface(constant.font(getApplicationContext()));
        setImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, IMG_RESULT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try{
            if(requestCode == IMG_RESULT && requestCode == RESULT_OK){
                Uri URI = data.getData();
                String [] File = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(URI, File, null, null, null);
                cursor.moveToFirst();
                int columnIndex = cursor.getColumnIndex(File[0]);
                ImageDecode = cursor.getString(columnIndex);
                cursor.close();
                imageselected.setImageBitmap(BitmapFactory
                        .decodeFile(ImageDecode));
                setImageselected(imageselected);
            }

        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "Please Try Again!", Toast.LENGTH_SHORT).show();
        }
    }
}