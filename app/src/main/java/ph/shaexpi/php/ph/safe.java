package ph.shaexpi.php.ph;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import ph.shaexpi.php.ph.model.constant;

/**
 * Created by on 10/7/2016.
 */

public class safe extends Activity {

    VideoView view;
    MediaController mediaController;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.safe);

        title = (TextView)findViewById(R.id.title);
        title.setText("Safe Sex");
        title.setTypeface(constant.font(safe.this));

        if(mediaController == null){
            mediaController = new MediaController(safe.this);
        }

        view = (VideoView)findViewById(R.id.video);
        try{
            view.setMediaController(mediaController);
            view.setVideoURI(Uri.parse("android.resource://" + safe.this.getPackageName() + "/" + R.raw.safe));
            view.start();
        }catch (Exception e){
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }
}
