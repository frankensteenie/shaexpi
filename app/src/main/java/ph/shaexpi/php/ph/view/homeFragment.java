package ph.shaexpi.php.ph.view;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;

/**K
 * Created on 7/21/2016.
 */
public class homeFragment extends Fragment {

    View v;

    private ProgressDialog pDialog;
    JSONParser jsonParser = new JSONParser();
    ArrayList<HashMap<String, String>> newStories;
    private static String url_campus_stories = "http://shaexpii.esy.es/getallcampus.php";
    private static String url_lesbian_stories = "http://shaexpii.esy.es/getalllesbian.php";
    private static String url_gay_stories = "http://shaexpii.esy.es/getallgay.php";
    private static String url_fantasy_stories = "http://shaexpii.esy.es/getallfantasy.php";
    private static String url_group_stories = "http://shaexpii.esy.es/getallgroup.php";
    private static String url_onenight_stories = "http://shaexpii.esy.es/getallonenight.php";
    private static String TAG_SUCCESS = "success";
    private static String TAG_STORIES = "campuses";
    private static String TAG_STORIES_LESBIAN = "lesbian";
    private static String TAG_STORIES_GAY = "gay";
    private static String TAG_STORIES_FANTASY = "fantasy";
    private static String TAG_STORIES_GROUP = "group";
    private static String TAG_STORIES_ONE = "onenight";
    private static String TAG_ID = "id";
    private static String TAG_TITLE = "title";
    JSONArray story = null;
    TextView storiescampus, storieslesbian, storiesgay, storiesfantasy, storiesgroup, storiesone;
    JSONArray tips = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.homefragment, container, false);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        newStories = new ArrayList<HashMap<String, String>>();

        new showcampusnewstories().execute();

        return v;
    }

    class showcampusnewstories extends AsyncTask<String, String, String>{

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Loading Home. Please wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(final String... args) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONObject json = jsonParser.makeHttpRequest(url_campus_stories, "GET", params);

                    try{
                        int success = json.getInt(TAG_SUCCESS);
                        if(success == 1){
                            tips = json.getJSONArray(TAG_STORIES);
                            for(int i = 0; i < tips.length(); i++){
                                JSONObject c = tips.getJSONObject(i);

                                String tip = c.getString(TAG_TITLE);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(TAG_TITLE, tip);

                                storiescampus = (TextView)v.findViewById(R.id.storiescampus);
                                storiescampus.setTypeface(constant.font(getContext()));
                                storiescampus.setTextSize(30);

                                storiescampus.setText(tip);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONObject json = jsonParser.makeHttpRequest(url_lesbian_stories, "GET", params);

                    try{
                        int success = json.getInt(TAG_SUCCESS);
                        if(success == 1){
                            tips = json.getJSONArray(TAG_STORIES_LESBIAN);
                            for(int i = 0; i < tips.length(); i++){
                                JSONObject c = tips.getJSONObject(i);

                                String tip = c.getString(TAG_TITLE);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(TAG_TITLE, tip);

                                storieslesbian = (TextView)v.findViewById(R.id.storieslesbian);
                                storieslesbian.setTypeface(constant.font(getContext()));
                                storieslesbian.setTextSize(30);

                                storieslesbian.setText(tip);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONObject json = jsonParser.makeHttpRequest(url_gay_stories, "GET", params);

                    try{
                        int success = json.getInt(TAG_SUCCESS);
                        if(success == 1){
                            tips = json.getJSONArray(TAG_STORIES_GAY);
                            for(int i = 0; i < tips.length(); i++){
                                JSONObject c = tips.getJSONObject(i);

                                String tip = c.getString(TAG_TITLE);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(TAG_TITLE, tip);

                                storiesgay = (TextView)v.findViewById(R.id.storiesgay);
                                storiesgay.setTypeface(constant.font(getContext()));
                                storiesgay.setTextSize(30);

                                storiesgay.setText(tip);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONObject json = jsonParser.makeHttpRequest(url_fantasy_stories, "GET", params);

                    try{
                        int success = json.getInt(TAG_SUCCESS);
                        if(success == 1){
                            tips = json.getJSONArray(TAG_STORIES_FANTASY);
                            for(int i = 0; i < tips.length(); i++){
                                JSONObject c = tips.getJSONObject(i);

                                String tip = c.getString(TAG_TITLE);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(TAG_TITLE, tip);

                                storiesfantasy = (TextView)v.findViewById(R.id.storiesfantasy);
                                storiesfantasy.setTypeface(constant.font(getContext()));
                                storiesfantasy.setTextSize(30);

                                storiesfantasy.setText(tip);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONObject json = jsonParser.makeHttpRequest(url_group_stories, "GET", params);

                    try{
                        int success = json.getInt(TAG_SUCCESS);
                        if(success == 1){
                            tips = json.getJSONArray(TAG_STORIES_GROUP);
                            for(int i = 0; i < tips.length(); i++){
                                JSONObject c = tips.getJSONObject(i);

                                String tip = c.getString(TAG_TITLE);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(TAG_TITLE, tip);

                                storiesgroup = (TextView)v.findViewById(R.id.storiesgroup);
                                storiesgroup.setTypeface(constant.font(getContext()));
                                storiesgroup.setTextSize(30);

                                storiesgroup.setText(tip);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    JSONObject json = jsonParser.makeHttpRequest(url_onenight_stories, "GET", params);

                    try{
                        int success = json.getInt(TAG_SUCCESS);
                        if(success == 1){
                            tips = json.getJSONArray(TAG_STORIES_ONE);
                            for(int i = 0; i < tips.length(); i++){
                                JSONObject c = tips.getJSONObject(i);

                                String tip = c.getString(TAG_TITLE);

                                HashMap<String, String> map = new HashMap<String, String>();
                                map.put(TAG_TITLE, tip);

                                storiesone = (TextView)v.findViewById(R.id.storiesone);
                                storiesone.setTypeface(constant.font(getContext()));
                                storiesone.setTextSize(30);

                                storiesone.setText(tip);
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            pDialog.dismiss();
        }
    }
}
