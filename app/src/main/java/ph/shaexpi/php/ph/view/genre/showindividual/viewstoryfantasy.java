package ph.shaexpi.php.ph.view.genre.showindividual;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.TextView;

import com.devspark.appmsg.AppMsg;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;

/**
 * Created by on 9/30/2016.
 */

public class viewstoryfantasy extends Activity {

    TextView title, story, date;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    String id;

    private static String urlccampusstory = "http://shaexpii.esy.es/getfantasyinstance.php";

    private static String TAG_SUCCESS = "success";
    private static String TAG_FANTASY = "fantasy";
    private static String TAG_ID = "id";
    private static String TAG_TIME = "dateadded";
    private static String TAG_TITLE = "title";
    private static String TAG_STORY = "story";

    AppMsg dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.read);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent i = getIntent();
        id = i.getStringExtra(TAG_ID);

        new viewstoryfantasy.storyfull().execute(urlccampusstory);
    }

    class storyfull extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(viewstoryfantasy.this);
            pDialog.setMessage("Loading Story. Please wait....");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(final String... args) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int success;
                    try{
                        List<NameValuePair> paramss = new ArrayList<NameValuePair>();
                        paramss.add(new BasicNameValuePair("id", id));

                        JSONObject json = jsonParser.makeHttpRequest(args[0], "GET", paramss);
                        Log.d("Single Story Details", json.toString());

                        success = json.getInt(TAG_SUCCESS);

                        if(success == 1){
                            JSONArray storyObj = json.getJSONArray(TAG_FANTASY);

                            JSONObject campusStory = storyObj.getJSONObject(0);

                            title = (TextView)findViewById(R.id.title);
                            title.setTypeface(constant.font(viewstoryfantasy.this));
                            title.setTextSize(25);
                            story = (TextView)findViewById(R.id.story);
                            story.setTypeface(constant.font(viewstoryfantasy.this));
                            story.setTextSize(15);

                            date = (TextView)findViewById(R.id.date);
                            date.setTypeface(constant.font(viewstoryfantasy.this));
                            date.setTextSize(20);

                            date.setText("Date Added: " + campusStory.getString(TAG_TIME));
                            title.setText(campusStory.getString(TAG_TITLE));
                            story.setText(campusStory.getString(TAG_STORY));
                        }else{
                            dialog.makeText(viewstoryfantasy.this, "Story not found", AppMsg.STYLE_INFO).show();
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            });
            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
            dialog.makeText(viewstoryfantasy.this, "Success get story: " + title.getText().toString(), AppMsg.STYLE_INFO).show();

            constant.playSound();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        constant.mp.stop();
    }

}
