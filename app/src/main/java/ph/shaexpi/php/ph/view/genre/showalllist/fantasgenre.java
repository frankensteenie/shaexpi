package ph.shaexpi.php.ph.view.genre.showalllist;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ph.shaexpi.php.ph.R;
import ph.shaexpi.php.ph.model.JSONParser;
import ph.shaexpi.php.ph.model.constant;
import ph.shaexpi.php.ph.view.genre.showindividual.viewstoryfantasy;

public class fantasgenre extends ListActivity {

    ListView list;

    TextView categorytext;

    private ProgressDialog progressDialog;

    JSONParser jsonParser = new JSONParser();

    ArrayList<HashMap<String, String>> fantasyTitle;

    private static String url_campus_stories = "http://shaexpii.esy.es/getallfantasy.php";

    private static String TAG_SUCCESS = "success";
    private static String TAG_STORIES = "fantasy";
    private static String TAG_ID = "id";
    private static String TAG_TITLE = "title";

    JSONArray campuss = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        fantasyTitle = new ArrayList<HashMap<String, String>>();

        categorytext = (TextView)findViewById(R.id.categorytext);
        categorytext.setTypeface(constant.font(fantasgenre.this));
        categorytext.setTextSize(20);
        categorytext.setText("Fantasy");

        list = getListView();

        new loadAllStories().execute();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long idd) {
                String id = ((TextView)view.findViewById(R.id.id)).getText().toString();
                Intent in = new Intent(getApplicationContext(), viewstoryfantasy.class);
                in.putExtra(TAG_ID, id);
                startActivityForResult(in, 100);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == 100){
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
    }

    class loadAllStories extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(fantasgenre.this);
            progressDialog.setMessage("Loading Stories. Please wait...");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jsonParser.makeHttpRequest(url_campus_stories, "GET", params);

            // Check your log cat for JSON response
            Log.d("All Stories: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    campuss = json.getJSONArray(TAG_STORIES);

                    // looping through All Products
                    for (int i = 0; i < campuss.length(); i++) {
                        JSONObject c = campuss.getJSONObject(i);

                        // Storing each json item in variable
                        String id = c.getString(TAG_ID);
                        String title = c.getString(TAG_TITLE);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_ID, id);
                        map.put(TAG_TITLE, title);

                        // adding HashList to ArrayList
                        fantasyTitle.add(map);
                    }
                } else {
                    // no products found
                    // Launch Add New product Activity
//                    Intent i = new Intent(getApplicationContext(),
//                            NewProductActivity.class);
                    // Closing all previous activities
//                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            progressDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            fantasgenre.this, fantasyTitle,
                            R.layout.campus, new String[] { TAG_ID,
                            TAG_TITLE},
                            new int[] { R.id.id, R.id.title });
                    // updating listview
                    setListAdapter(adapter);
                }
            });
        }
    }
}
