package ph.shaexpi.php.ph.model;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;

import java.util.Random;

import ph.shaexpi.php.ph.R;

/**
 * Created on 7/29/2016.
 */
public class constant {

    static Context context;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        constant.context = context;
    }

    public static String isEmpty = " ";

    public static Random rand = new Random();
    public static MediaPlayer mp;

    public static int index;

    public static int max = 12;
    public static int min = 1;

    public static int[] sounds = {R.raw.aubrey, R.raw.earnedit, R.raw.genasis, R.raw.gnash, R.raw.jeremih, R.raw.layme,
            R.raw.littlemix, R.raw.loveme, R.raw.numb, R.raw.onthewings, R.raw.photograph, R.raw.sayo, R.raw.eros, R.raw.lemongrass, R.raw.inyourroom, R.raw.mybeloved,
            R.raw.music, R.raw.passion, R.raw.specialerotic, R.raw.wickedgames, R.raw.tango};

    public static Typeface font(Context context) {
        constant.context = context;
        Typeface font = Typeface.createFromAsset(context.getAssets(), "FallingSky.otf");
        return font;
    }

    public static Typeface fontTextView(Context context) {
        constant.context = context;
        Typeface font = Typeface.createFromAsset(context.getAssets(), "FallingSky.otf");
        return font;
    }

    public static void playSound() {
        int index = constant.rand.nextInt(max - min);
        for (int i = 0; i < sounds.length; i++) {
            shuffleArray(sounds);
            constant.mp = MediaPlayer.create(context, constant.sounds[index]);
        }
        constant.mp.start();
    }

    public static void shuffleArray(int[] arr) {
        Random random = new Random();
        for (int i = arr.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);
            if (index != i) {
                arr[index] ^= arr[i];
                arr[i] ^= arr[index];
                arr[index] ^= arr[i];
            }
        }
    }

    public static String[] foulWords = {"anus","arse","arsehole","ass",
            "asshat","asspirate","assjabber","assbag",
            "assbanger","assbite","assclown","asscock",
            "asscracker","asses","assface","assfuck",
            "assfucker","assgoblin","asshat","asshead",
            "asshole","asshopper","assjacker","asslick",
            "asslicker","assmonkey","assmunch","assmuncher",
            "assnigger","asspirate","assshit","assshole",
            "asssucker","asswad","asswipe","axwound",
            "bampot","bastard ","beaner","bitch",
            "bitchass","bitches","bitchtits ","bitchy ",
            "blow job","blowjob ","bollocks ","bollox",
            "boner ","brotherfucker ","bullshit ","bumblefuck ",
            "butt plug ","buttpirate","buttfucka ","buttfucker ",
            "camel toe","carpetmuncher ","chesticle","chinc ",
            "chink","choad ","chode ","clit ",
            "clitface ","clitfuck ","clusterfuck ","cock ",
            "cockass ","cockbite ","cockburger ","cockface ",
            "cockfucker ","cockhead","cockjockey ","cockknoker ",
            "cockmaster ","cockmongler ","cockmongruel ","cockmonkey ",
            "cockmuncher ","cocknose ","cocknugget ","cockshit ",
            "cocksmith ","cocksmoke ","cocksmoker ","cocksniffer ",
            "cocksucker ","cockwaffle ","coochie ","coochy ",
            "coon ","cooter ","cracker ","cum ",
            "cumbubble ","cumdumpster ","cumguzzler ","cumjockey ",
            "cumslut ","cumtart ","cunnie ","cunnilingus ",
            "cunt ","cuntass ","cuntface ","cunthole ",
            "cuntlicker ","cuntrag","cuntslut","dago ",
            "damn","deggo ","dick ","dicksneeze",
            "dickbag ","dickbeaters ","dickface ","dickfuck ",
            "dickfucker ","dickhead ","dickhole ","dickjuice ",
            "dickmilk ","dickmonger ","dicks ","dickslap ",
            "dicksucker","dicksucking ","dicktickler ","dickwad ",
            "dickweasel ","dickweed ","dickwod ","dike ",
            "dildo ","dipshit ","doochbag ","dookie ",
            "douche ","douchefag","douchebag ","douchewaffle ",
            "dumass ","dumb ass","dumbass ","dumbfuck ",
            "dumbshit ","dumshit ","dyke ","",
            "fag ","fagbag ","fagfucker ","faggit ",
            "faggot ","faggotcock ","fagtard ","fatass ",
            "fellatio ","feltch ","flamer ","fuck ",
            "fuckass ","fuckbag ","fuckboy ","fuckbrain ",
            "fuckbutt ","fuckbutter ","fucked ","fucker ",
            "fuckersucker ","fuckface ","fuckhead ","fuckhole ",
            "fuckin ","fucking ","fucknut ","fuckoff ",
            "fucks ","fuckstick ","fucktard ","fucktart ",
            "fuckup ","fuckwad ","fuckwit ",
            "fuckwitt ","fudgepacker ","gay ","gayass ",
            "gaybob ","gaydo ","gayfuck ","gayfuckist ",
            "gaylord ","gaytard ","gaywad ","goddamn ",
            "goddamnit ","gooch ","gook ","gringo ","guido ",
            "handjob ","hard on","heeb ","hell ","ho ",
            "hoe ","homo ","homodumbshit ","honkey ","humping",
            "jackass ","jagoff ","jap ","jerk off","jerkass ",
            "jigaboo ","jizz ","jungle bunny","junglebunny ","kike ",
            "kooch ","kootch ","kraut ","kunt ","kyke ",
            "lameass ","lardass ","mcfagget  ","lesbo ","lezzie ",
            "mick ","minge ","mothafucka ","mothafuckin","motherfucker ",
            "motherfucking ","muff ","muffdiver ","munging ","negro ",
            "nigaboo ","nigga ","nigger ","niggers ","niglet ",
            "nut sack","nutsack ","paki ","panooch ","pecker ",
            "peckerhead ","penis ","penisbanger ","penisfucker ","penispuffer ",
            "piss ","pissed ","pissed off","pissflaps ","polesmoker ",
            "pollock ","poon ","poonani ","poonany ","poontang",
            "porch monkey","porchmonkey ","prick ","punanny ","punta ",
            "pussies ","pussy ","pussylicking ","puto ","queef ",
            "queer ","queerbait ","queerhole ","renob ","rimjob ",
            "ruski ","sand nigger","sandnigger ","schlong ","scrote ",
            "shit ","shitass ","shitbag ","shitbagger ","shitbrains ",
            "shitbreath ","shitcanned ","shitcunt ","shitdick ","shitface ",
            "shitfaced ","shithead ","shithole ","shithouse ","shitspitter ",
            "shitstain ","shitter ","shittiest ","shitting ","shitty ",
            "shiz ","shiznit ","skank ","skeet ","skullfuck ",
            "slut ","slutbag","smeg ","snatch ","spic ",
            "spick ","splooge ","spook ","suckass ","tard ",
            "testicle ","thundercunt ","tit ","titfuck ","tits ",
            "tittyfuck ","twat ","twatlips ","twats ","twatwaffle ",
            "unclefucker ","vajj","vag ","vagina ","vajayjay ",
            "vjayjay ","wank ","wankjob ","wetback ","whore ",
            "whorebag ","whoreface ","wop ", "tira", "titi",
            "pepe", "puday", "puke", "bayag", "kinantot", "kande", "kinande",
            "kantutin", "chupa", "lambe", "burat", "kasta", "kinasta", "kumasta",
            "bayo ", "binayo ", "bumabayo ", "binabayo ", "kayog ", "kinayog ", "kinakayog ", "chumuchupa ", "nangchuchupa ", "tinuchupa ", "tsumupa ",
            "brocaha ", "binobrocha ", "bumobrucha ", "kinakande ", "kakandiin ", "tumbong ", "tingel ", "tinggil ", "hima ", "tumbong ",
            "Chupa", "chumuchupa", "chinuchupa", "chinupa", "chumupa", "tsupa", "tsumutsupa", "tsinutsupa", "tsumupa", "tsinupa", "tiyupa", "tinutiyupa",
            "Kantot", "Kinantot", "kinakantot", "kumantot", "kantutan", "kumakantot", "Kantot", "tite"};
}
